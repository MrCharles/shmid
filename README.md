# shmid

Small user utility for obscured temp directories.

-----

### Install

```bash
$> make help
$> make install
$> ls -a ~/bin | grep shmid
genshmid
getshmid
```

-----
-----

### Purpose

These directories are used for ... whatever purpose a user has for them.

For example, I don't want SHMID regenerated at a user-global scope every time a console session is initiated. Only usually after a fresh reboot, or upon demand.

Thus, using OpenBox, my `~/.config/openbox/autostart` script has a line: `~/bin/genshmid --rip &`

Such that the previous `~/.shmid` is overwritten with a new instance, and previous instances are removed as cleanly as possible.

If you have running processes hosting temporary directories inside of a previous instance when calling `~/bin/genshmid --rip` ... an `rm -rf` is performed on the previous instance without regard to moving things around _(implicit clean-up)_. This may cause issues. So, before running this script, ensure that nothing is running, saved, hosted, etc, in the directories it creates.

Thereafter, `~/bin/getshmid` will return the contents of `~/.shmid`, with `echo $(cat ~/shmid)`, or `0` if that file does not exist.

In turn, the directories created are:

* `/tmp/$(cat ~/.shmid)`
* `/dev/shm/$(cat ~/.shmid)`

If you manually run `~/bin/genshmid` without the `--rip` parameter, it should not replace existing. Providing the `--rip` parameter means you specifically intend to replace any previous `~/.shmid`
