
all:
	@echo "Use 'make install'"

default:
	@echo "Use 'make install'"

help:
	@echo "shmid -- small utility for obscured temp directories"
	@echo "Installs bash scripts to the user bin directory, ~/bin/getshmid, ~/bin/genshmid"
	@echo "Use 'make install' to install."
	@echo "No uninstall mechanism provided."
	@echo "To remove, simply delete the two files installed."

install:
	cp src/getshmid ~/bin/getshmid
	chmod +x ~/bin/getshmid
	chmod og-rwx ~/bin/getshmid
	cp src/getshmid ~/bin/genshmid
	chmod +x ~/bin/genshmid
	chmod og-rwx ~/bin/genshmid
	~/bin/genshmid



